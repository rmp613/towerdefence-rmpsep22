﻿using UnityEngine;
using System.Collections;

public class Sniper : BaseWeapon {

	GameObject ch;
	public int currentAmmo = 10; 
	public int currentExtraAmmo = 10;
	public int maxClipSize = 20;
	public int maxExtraAmmo = 20;

	bool isFiring = false;
	bool isADS = false;

	// Use this for initialization
	void Start ()
	{
		gunAnim = GetComponent<Animation> ();
		gunAnim.Play ("Draw");
		ch = GameObject.FindGameObjectWithTag ("Crosshair");
		ch.SetActive (true);
		gunSound = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update ()
	{
		if (!gunAnim.isPlaying) {
			//Debug.Log ("ANIMATION IS NOT PLAYING");
			if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0))) {
				isFiring = true;
			} else if ((Input.GetButtonUp ("Fire1") || Input.GetMouseButtonUp (0))) {
				isFiring = false;
			}

			//Own Reload bit
			if (Input.GetKeyDown (KeyCode.R)) {
				gunAnim.Play ("Reload");
				gunAnim.PlayQueued ("Unload");
				GlobalAmmo.currentExtraAmmo = GlobalAmmo.currentAmmo - GlobalAmmo.maxClipSize;
				GlobalAmmo.currentAmmo = GlobalAmmo.maxClipSize;
			}


			if (Input.GetKeyDown (KeyCode.R) && GlobalAmmo.currentExtraAmmo > 0 && GlobalAmmo.currentAmmo < GlobalAmmo.maxClipSize) {
				Reload ();
			}

			if (Input.GetButtonDown ("Fire2")) {
				if (isADS) {
					gunAnim.Play ("UnADS");
					isADS = false;
				} else {
					gunAnim.Play ("ADS");
					isADS = true;
				}
			}


			if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0)) && GlobalAmmo.currentAmmo > 0) {
				AudioSource gunSound = GetComponent<AudioSource> ();
				if (isADS) {
					gunAnim.Play ("ADS Fire");
					gunAnim.PlayQueued ("UnADS");
					gunAnim.PlayQueued ("Unload");
					gunAnim.PlayQueued ("ADS");
				} else {
					gunAnim.Play ("Hipfire");
					gunAnim.PlayQueued ("Unload");
				}
				gunSound.Play ();
				GlobalAmmo.currentAmmo--;

			}
		}
	}

	public void Reload ()
	{
		GlobalAmmo.currentAmmo = GlobalAmmo.currentExtraAmmo + GlobalAmmo.currentAmmo;

		if (GlobalAmmo.currentAmmo >= GlobalAmmo.maxClipSize) {
			gunAnim.Play ("Reload");
			gunAnim.PlayQueued ("Unload");
			GlobalAmmo.currentExtraAmmo = GlobalAmmo.currentAmmo - GlobalAmmo.maxClipSize;
			GlobalAmmo.currentAmmo = GlobalAmmo.maxClipSize;
		}
	}
}
