﻿using UnityEngine;
using System.Collections;

public class BackgroundMusicScript : MonoBehaviour
{

	int thirdTierLimit = 5;

	public AudioClip firstTierClip, secondTierClip, thirdTierClip;
	private AudioSource source;

	// Use this for initialization
	void Start ()
	{
		source = GetComponent<AudioSource> ();
		source.clip = firstTierClip;
		source.Play ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		//If no shots have been fired (idle gameplay) - play the normal music.
		if (Statistics.shotsFired == 0) {
			if (source.clip.GetInstanceID () != firstTierClip.GetInstanceID ()) {
				source.Stop ();
				source.clip = firstTierClip;
				source.Play ();
			}
		} else {
			//If still within the time between shots to play fast paced music...
			if (Statistics.timeWithoutShots < 5) {
				//If enough shots have been fired within several seconds...
				if (Statistics.shotsFired >= thirdTierLimit ) {
					//Unless the clip desired is already playing...
					if (source.clip.GetInstanceID () != thirdTierClip.GetInstanceID ()) {
						//Play the fastest paced music.
						source.Stop ();
						source.clip = thirdTierClip;
						source.Play ();
					}
				} else {
					//Play the second fastest paced music.
					if (source.clip.GetInstanceID () != secondTierClip.GetInstanceID ()) {
						source.Stop ();
						source.clip = secondTierClip;
						source.Play ();
					}
				}
			}
		}
	}

}
