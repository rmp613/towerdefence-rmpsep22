﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class BaseHealth : NetworkBehaviour
{
	[SerializeField]
	protected float _maxHealth; 
	public float maxHealth { 
		get { return _maxHealth; }
		set { _maxHealth = value; }
	}

	[SyncVar] 
	protected float _currentHealth; 
	public float currentHealth { 
		get { return _currentHealth; }
		set { _currentHealth = value; }
	}
   
    void Awake() {
        Setup();
    }

	public virtual void Setup() {
        currentHealth = maxHealth;
    }

    [Command]
	public virtual void CmdTakeDamage(float _amount)
	{
		//Override this for more complex actions
		currentHealth -= _amount;

		if(currentHealth <= 0)
		{
			Die();
		}
	}

    [Command]
    public virtual void CmdHealDamage(float _amount)
	{
		//Override this for more complex actions
		currentHealth = Mathf.Min(currentHealth + _amount, maxHealth);
	}


	protected virtual void Die()
	{
		//TODO: determine if I can leave the defaul die inside this class
		Destroy(gameObject);
	}

}

