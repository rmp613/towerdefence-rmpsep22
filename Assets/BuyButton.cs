﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuyButton : MonoBehaviour {
	bool isBought;
	bool canBuy;
	int price;
	public GameObject labelText;
	public Button button;
	// Use this for initialization
	void Start () {
		labelText.GetComponent<Text> ().text = "Buy";
	}

	public void SetCanBuy(bool isBuyable){
		canBuy = isBuyable;
		if (canBuy) {
			labelText.GetComponent<Text> ().text = "Buy";
		} else {
			this.GetComponent<Image> ().color = Color.red;
			labelText.GetComponent<Text> ().text = "Unavailable";
			button.interactable = false;
		}
	}

	public void SetPrice(int price){
		this.price = price;
	}

	// Update is called once per frame
	void Update () {
		
	}


	protected virtual void OnMouseDown(){
		GameObject player = GameObject.FindGameObjectWithTag ("Player");
		if (player != null) {
			player.GetComponent<Player> ().funds -= price;
		}
		labelText.GetComponent<Text> ().text = "Bought";
		button.interactable = false;
		//Overridden in child classes
	}


	public bool IsBought(){
		return isBought;
	}
}
